import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import net.miginfocom.swing.MigLayout;
import javax.swing.BoxLayout;
import java.awt.CardLayout;


/**
 * AppWindow
 * Main application class, tide up together back-end and interface
 * 
 * This project is also used as an base for Javadocs
 *
 * @author Kuba Domagalski
 * @version 1.00, 24 Jan 2019
 */
public class AppWindow {

	private JFrame frame;
	private JTable table;
	private JTextField xTextField;
	private JTextField yTextField;
	private JTextField frameTextField;
	private JTextField fieldTextField;
	private List<Point> points = new ArrayList<>();
	private MyJPanel imagePanel;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AppWindow window = new AppWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * AppWindow
	 * 
	 * public method for interface initialization
	 * @since version 1.00
	 */
	public AppWindow() {
		initialize();
	}

	/**
	 * initialize
	 * 
	 * contains all information about application interface layout
	 * @since version 1.00
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1126, 609);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		panel.setPreferredSize(new Dimension(200, 100));
		panel.setAlignmentY(Component.TOP_ALIGNMENT);
		panel.setAlignmentX(Component.LEFT_ALIGNMENT);
		frame.getContentPane().add(panel, BorderLayout.WEST);
		panel.setLayout(new GridLayout(2, 1));
		
		JScrollPane pointListPanel = new JScrollPane();
		panel.add(pointListPanel);
		pointListPanel.setAlignmentY(Component.BOTTOM_ALIGNMENT);
		pointListPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		table = new JTable(new DefaultTableModel(new Object[]{"X","Y"}, 0));
		table.setBorder(new LineBorder(new Color(0, 0, 0)));
		pointListPanel.setViewportView(table);
		
		JPanel buttonPanel = new JPanel();
		panel.add(buttonPanel);
		buttonPanel.setAlignmentY(Component.TOP_ALIGNMENT);
		buttonPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		buttonPanel.setLayout(new GridLayout(0, 1, 0, 5));
		
		JPanel newPointPanel = new JPanel();
		buttonPanel.add(newPointPanel);
		
		JPanel newPointLabelPanel = new JPanel();
		newPointLabelPanel.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel lblNewLabel = new JLabel("X");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		newPointLabelPanel.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Y");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		newPointLabelPanel.add(lblNewLabel_1);
		
		JPanel newPointTextPanel = new JPanel();
		newPointTextPanel.setLayout(new GridLayout(0, 2, 0, 0));
		
		xTextField = new JTextField();
		newPointTextPanel.add(xTextField);
		xTextField.setColumns(10);
		
		yTextField = new JTextField();
		newPointTextPanel.add(yTextField);
		yTextField.setColumns(10);
		newPointPanel.setLayout(new GridLayout(3, 2, 0, 5));
		newPointPanel.add(newPointLabelPanel);
		newPointPanel.add(newPointTextPanel);
		
		JButton addPointButton = new JButton("Dodaj punkt");
		addPointButton.addActionListener(new ActionListener() {
			/**
			 * addPointButton actionPerformed
			 * 
			 * function that calls savePoint function
			 * @see #savePoint
			 * @since version 1.00
			 */
			public void actionPerformed(ActionEvent arg0) {
				savePoint();
			}
		});
		newPointPanel.add(addPointButton);
		
		JPanel figureInfoPanel = new JPanel();
		buttonPanel.add(figureInfoPanel);
		figureInfoPanel.setLayout(new GridLayout(3, 2, 10, 5));
		
		JPanel framePanel = new JPanel();
		figureInfoPanel.add(framePanel);
		framePanel.setLayout(new GridLayout(0, 2, 0, 0));
		
		frameTextField = new JTextField();
		frameTextField.setEditable(false);
		framePanel.add(frameTextField);
		frameTextField.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Obw\u00F3d");
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		framePanel.add(lblNewLabel_2);
		
		JPanel fieldPanel = new JPanel();
		figureInfoPanel.add(fieldPanel);
		fieldPanel.setLayout(new GridLayout(0, 2, 0, 0));
		
		fieldTextField = new JTextField();
		fieldTextField.setEditable(false);
		fieldPanel.add(fieldTextField);
		fieldTextField.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("Pole");
		lblNewLabel_3.setHorizontalAlignment(SwingConstants.CENTER);
		fieldPanel.add(lblNewLabel_3);
		
		JButton countButton = new JButton("Oblicz");
		countButton.addActionListener(new ActionListener() {
			/**
			 * countButton actionPerformed
			 * 
			 * function that checks if provided number of point is greater or equal 3 and if is starts
			 * CountField and CountPerimeter functions
			 * @see #CountField
			 * @see #CountPerimeter
			 * @since version 1.00
			 */
			public void actionPerformed(ActionEvent e) {
				
				if (points.size() < 3) {
					return;
				}
				
				CountField();
				CountPerimeter();
			}

			/**
			 * CountPerimeter
			 * 
			 * function that count perimeter of a figure based on points provided by user 
			 * @since version 1.00
			 */
			private void CountPerimeter() {
				float perimeterSum = 0;

				for (int i = 0; i < points.size(); i++) {
					Point nextPoint = null;
					
					if (i == points.size()-1) {
						nextPoint = points.get(0);
					} else {
						nextPoint = points.get(i+1);
					}
					
					int x1 = nextPoint.x;
					int x2 = points.get(i).x;
					
					int y1 = nextPoint.y;
					int y2 = points.get(i).y;
					
					perimeterSum += Math.abs(Math.sqrt((y2 - y1) * (y2 - y1) + (x2 - x1) * (x2 - x1)));
				}
				
				frameTextField.setText(String.valueOf(perimeterSum));
			}

			/**
			 * CountField
			 * 
			 * function that count field in based on point given by application user
			 * @since version 1.00
			 */
			private void CountField() {
				float poleSuma = 0;

				for (int i = 0; i < points.size(); i++) {
					Point nextPoint = null;
					Point prevPoint = null;
					if (i == 0) {
						 prevPoint = points.get(points.size()-1);
					} else {
						 prevPoint = points.get(i-1);
					}
					
					if (i == points.size()-1) {
						nextPoint = points.get(0);
					} else {
						nextPoint = points.get(i+1);
					}
					
					poleSuma += (nextPoint.x - prevPoint.x) * points.get(i).y;
				}
				
				poleSuma /= 2;
				
				fieldTextField.setText(String.valueOf(Math.abs(poleSuma)));
			}
		});
		figureInfoPanel.add(countButton);
		
		imagePanel = new MyJPanel();
		imagePanel.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		frame.getContentPane().add(imagePanel, BorderLayout.CENTER);
		imagePanel.setLayout(null);
		imagePanel.repaint();
	}

	/**
	 * savePoint
	 * 
	 * function that validate user input and if valid stores passed x and y point coordinate
	 * @since version 1.00
	 */
	protected void savePoint() {
		String x = xTextField.getText();
		String y = yTextField.getText();
		
		try {
			int xInt = Integer.parseInt(x);
			int yInt = Integer.parseInt(y);
			
			DefaultTableModel model = (DefaultTableModel) table.getModel();
			model.addRow(new Object[]{x, y});	
			Point point = new Point();
			point.setLocation(xInt, yInt);
			points.add(point);
			imagePanel.setPoints(points);
			imagePanel.repaint();
		} catch (NumberFormatException e) {
			return;
		}
		yTextField.setText("");
		xTextField.setText("");
	}
}
