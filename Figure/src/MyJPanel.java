import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

/**
 * custom JPanel class that exdends JPanel
 * 
 * contains all methods necessary to properly chart area and figure
 * 
 * @author Kuba Domagalski
 * @version 1.00, 24 Jan 2019
 *
 */
public class MyJPanel extends JPanel {
	
	List<Point> points = new ArrayList<>();
	private int maxPoint; 

	/**
	 * paintComponent
	 * 
	 * main function used to paint whole chart area
	 * @param g graphics component used for painting on JPanel 
	 * @since version 1.00
	 */	
	@Override	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		redraw(g);
	}

	/**
	 * redraw
	 * 
	 * function used for redrawing chart area to reflect changes
	 * 
	 * @see #drawAxes
	 * @see #drawCrissCross
	 * @see #drawShape
	 * @param g graphics component used for painting on JPanel 
	 * @since version 1.00
	 */
	private void redraw(Graphics g) {
		setBackground(Color.white);
		
		maxPoint = calcScale();
		
		drawAxes(g);
		
		drawCrissCross(g);
		
		drawShape(g);
	}

	/**
	 * calcScale
	 * 
	 * calculate scale that has to be used in painting figures on chart area, 
	 * it takes in consideration JPanel size in pixels
	 * 
	 * @return scale used in all figure related graphics actions 
	 * @since version 1.00
	 */
	private int calcScale() {
		int maxPoint = 0;
		
		for (Point point : points) {
			maxPoint = point.y > maxPoint ? point.y : maxPoint;
			maxPoint = point.x > maxPoint ? point.x : maxPoint;
		}
		
		return maxPoint;
	}

	/**
	 * drawAxes
	 * 
	 * draws axles onto JPanel
	 * 
	 * @param g graphics component used for painting on JPanel 
	 * @since version 1.00
	 */
	private void drawAxes(Graphics g) {
		g.setColor(Color.DARK_GRAY);
		
		int thickness = 3;
		
		g.fillRect(this.getWidth()/2 - thickness/2, 0, thickness, this.getHeight());
		g.fillRect(0, this.getHeight()/2 - thickness/2, this.getWidth(), thickness);
	}

	/**
	 * drawCrissCross
	 * 
	 * draws crisscross onto JPanel
	 * 
	 * @param g graphics component used for painting on JPanel 
	 * @since version 1.00
	 */
	private void drawCrissCross(Graphics g) {
		g.setColor(Color.GRAY);
		
		int linesCount = 10;
		
		int roundedTenth = this.getWidth()/linesCount;
		
		for (int i = 0; i < linesCount; i++) {
			g.drawLine(this.getWidth()/10 * (i + 1), 0, this.getWidth()/10 * (i + 1), this.getHeight());
			g.drawLine(0, this.getHeight()/10 * (i + 1), this.getWidth(), this.getHeight()/10 * (i + 1));
		}
	}

	/**
	 * drawShape
	 * 
	 * draws points and lines that makes a figure onto JPanel
	 * 
	 * @param g graphics component used for painting on JPanel 
	 * @since version 1.00
	 */
	private void drawShape(Graphics g) {
		g.setColor(Color.BLACK);
		for (int i = 0; i < points.size(); i++) {
			Point nextPoint = null;
			
			if (i == points.size()-1) {
				nextPoint = points.get(0);
			} else {
				nextPoint = points.get(i+1);
			}
			
			int SCALE_X = (int) Math.round(this.getHeight()/2 - 0.1*this.getHeight()/2);
			int SCALE_Y = (int) Math.round(this.getWidth()/2 - 0.1*this.getWidth()/2);
			int moveFactorX = this.getWidth()/2;
			int moveFactorY = this.getHeight()/2;
			
			
			int xNextPointAfterScale = Math.round(((nextPoint.x * SCALE_Y )/ maxPoint) + moveFactorX);
			int yNextPointAfterScale = Math.round(((nextPoint.y * SCALE_X )/ maxPoint) - moveFactorY) * -1;
			
			int xAfterScale = Math.round(((points.get(i).x * SCALE_Y )/ maxPoint) + moveFactorX);
			int yAfterScale = Math.round(((points.get(i).y * SCALE_X )/ maxPoint) - moveFactorY) * -1;
			
			int pointThickness = 8;
			
			g.fillOval(xAfterScale-pointThickness/2, yAfterScale-pointThickness/2, pointThickness, pointThickness);
			g.drawLine(xAfterScale, yAfterScale, xNextPointAfterScale, yNextPointAfterScale);
		}
	}

	/**
	 * getPoints 
	 * 
	 * @return list of points used in chart drawing
	 * @since version 1.00
	 */
	public List<Point> getPoints() {
		return points;
	}

	/**
	 * setPoints
	 * 
	 * @param points list of point used for figure drawing
	 */
	public void setPoints(List<Point> points) {
		this.points = points;
	}
}
